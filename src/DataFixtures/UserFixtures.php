<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    public function __construct(private UserPasswordHasherInterface $passwordEncoder)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $items = self::getDatas();

        foreach ($items as $item) {

            $entity = new User();
            $password = $this->passwordEncoder->hashPassword($entity, $item['password']);
            $entity->setPassword($password);
            $entity->setEmail($item['email']);
            $entity->setRoles($item['roles']);
            $manager->persist($entity);
        }
        $manager->flush();
    }

    public static function getDatas(): array
    {
        return [
            [
                'email' => 'fabrice@touch-agency.net',
                'password' => 'fabrice',
                'roles' => ['ROLE_ADMIN'],
            ],
        ];
    }
}
