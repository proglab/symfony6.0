<?php

namespace App\Security;

use App\Form\LoginFormType;
use App\Repository\UserRepository;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\UX\Turbo\Stream\TurboStreamResponse;
use Twig\Environment;

class WpaAuthenticator extends AbstractAuthenticator
{
    private $form = null;

    public function __construct(private UserRepository $userRepository, private FormFactoryInterface $formFactory, private Environment $twig)
    {
        $this->form = $this->formFactory->create(LoginFormType::class);
    }

    public function supports(Request $request): ?bool
    {
        return ($request->getPathInfo() === '/login' && $request->isMethod('POST'));
    }

    public function authenticate(Request $request): Passport
    {
        $this->form->handleRequest($request);
        if ($this->form->isSubmitted() && $this->form->isValid()) {
            $password = $this->form->get('password')->getData();
            $username = $this->form->get('username')->getData();
            return new Passport(
                new UserBadge($username),
                new PasswordCredentials($password)
            );


        } else {
            dd($this->form->getErrors());
        }
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        if (TurboStreamResponse::STREAM_FORMAT === $request->getPreferredFormat()) {
            $htmlContents = $this->twig->render('login/success.stream.html.twig', []);
            $response = new Response();
            $response->setContent($htmlContents);
            $response->setStatusCode(TurboStreamResponse::HTTP_OK);
            $response->headers->set('Content-Type', TurboStreamResponse::STREAM_MEDIA_TYPE);
            return $response;
        }

        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        if (TurboStreamResponse::STREAM_FORMAT === $request->getPreferredFormat()) {
            $htmlContents = $this->twig->render('login/error.stream.html.twig', ['form' => $this->form->createView(), 'errors' => $this->form->getErrors()]);
            $response = new Response();
            $response->setContent($htmlContents);
            $response->setStatusCode(TurboStreamResponse::HTTP_UNPROCESSABLE_ENTITY);
            $response->headers->set('Content-Type', TurboStreamResponse::STREAM_MEDIA_TYPE);
            return $response;
        }

        throw new CustomUserMessageAuthenticationException('user inactive');
    }

//    public function start(Request $request, AuthenticationException $authException = null): Response
//    {
//        /*
//         * If you would like this class to control what happens when an anonymous user accesses a
//         * protected page (e.g. redirect to /login), uncomment this method and make this class
//         * implement Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface.
//         *
//         * For more details, see https://symfony.com/doc/current/security/experimental_authenticators.html#configuring-the-authentication-entry-point
//         */
//    }
}
